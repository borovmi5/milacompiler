#include "Lexan.h"

#include <iostream>
#include <cstdarg>

using namespace std;

bool CharGrouping::IsNumeric(char c, BASE b)
{
    return (c >= '0' && c <= '7')
           || (b != BASE::OCT && c >= '8' && c <= '9')
           || (b == BASE::HEX && ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'f')));
}

bool CharGrouping::IsUpper(char c)
{
    return (c >= 'A' && c <= 'Z');
}

bool CharGrouping::IsLower(char c)
{
    return (c >= 'a' && c <= 'z');
}

bool CharGrouping::IsAlpha(char c)
{
    return IsUpper(c) || IsLower(c);
}

bool CharGrouping::IsAlphaNumeric(char c)
{
    return IsAlpha(c) || IsNumeric(c);
}

bool CharGrouping::IsSpecial(char c)
{
    for (int idx = 0; s_Special[idx] != '\0'; ++idx)
        if (c == s_Special[idx])
            return true;
    return false;
}

bool CharGrouping::IsWhiteSpace(char c)
{
    for (int idx = 0; s_Whitespace[idx] != '\0'; ++idx)
        if (c == s_Whitespace[idx])
            return true;
    return false;
}

bool CharGrouping::IsDelimiter(char c)
{
    return IsWhiteSpace(c) || IsSpecial(c);
}

bool CharGrouping::IsUnknown(char c)
{
    return !IsDelimiter(c) && !IsAlphaNumeric(c) && c != '_';
}

const char CharGrouping::s_Special[] = ";()+-*<>=:[],.";
const char CharGrouping::s_Whitespace[] = " \t\n";

Lexan::Lexan(const string& filename) :
    m_FileName(move(filename)),
    m_Row(1),
    m_Col(1)
{
    m_SourceFile.open(m_FileName, fstream::in);
}

Lexan::~Lexan()
{
    if(m_SourceFile.is_open())
        m_SourceFile.close();
}

bool Lexan::eof() const
{
    return m_SourceFile.eof();
}

Lexan::LEXEM Lexan::NextToken()
{
    string name;
    int val = 0;
    BASE base = DEC;

    SkipWhiteSpace();
    if(eof())
        return END_OF_FILE;

    char symbol = Peek();
    m_LastRow = m_Row;
    m_LastCol = m_Col;

    using CG = CharGrouping;

    if(CG::IsAlpha(symbol) || symbol == '_')
        goto lID;
    else if(symbol == OCT_PREFIX)
    {
        Get();
        symbol = Peek();
        base = OCT;
        goto lOct;
    }
    else if(symbol == HEX_PREFIX)
    {
        Get();
        symbol = Peek();
        base = HEX;
        goto lHex;
    }
    else if(CG::IsNumeric(symbol))
        goto lDec;
    else
    {
        Get();
        char second_symbol = Peek();
        SkipWhiteSpace();
        switch(symbol)
        {
            case '+':
                return SYMB_ADD;
            case '-':
                return SYMB_SUB;
            case '*':
                return SYMB_MUL;
            case '^':
                return SYMB_POW;
            case '<':
                if(second_symbol == '=')
                {
                    Get();
                    SkipWhiteSpace();
                    return SYMB_LE;
                }
                else if(second_symbol == '>')
                {
                    Get();
                    SkipWhiteSpace();
                    return SYMB_NEQ;
                }
                return SYMB_LT;
            case '>':
                if(second_symbol == '=')
                {
                    Get();
                    SkipWhiteSpace();
                    return SYMB_GE;
                }
                return SYMB_GT;
            case '=':
                return SYMB_EQ;
            case '(':
                return SYMB_LPAR;
            case ')':
                return SYMB_RPAR;
            case '[':
                return SYMB_LSB;
            case ']':
                return SYMB_RSB;
            case ':':
                if(second_symbol == '=')
                {
                    Get();
                    SkipWhiteSpace();
                    return SYMB_ASS;
                }
                return SYMB_COL;
            case ',':
                return SYMB_COM;
            case '.':
                if(second_symbol == '.')
                {
                    Get();
                    SkipWhiteSpace();
                    return SYMB_RNG;
                }
                return SYMB_DOT;
            case ';':
                return SYMB_SCOL;
            default:
            {
                Error(ERR_UNKNOWN_SYMB, symbol, symbol);
                return UNKNOWN;
            }
        }
    }

lID:
    while(true)
    {
        if(CG::IsDelimiter(symbol) || eof() || CG::IsUnknown(symbol))
        {
            if(CG::IsNumeric(name[0]))
                Error(ERR_INVALID_ID, name.c_str());
            auto token = s_Keywords.find(name);
            if(token == s_Keywords.end())
                return { IDENTIFIER, name.c_str() };
            return token->second;
        }
        name.push_back(Get());
        symbol = Peek();
    }

lDec:
    while(true)
    {
        if(CG::IsDelimiter(symbol) || eof())
            return { NUMBER, val };
        else if(!CG::IsNumeric(symbol))
            goto lID;
        name.push_back(Get());
        val *= base;
        val += symbol - '0';
        symbol = Peek();
    }

lHex:
    while(true)
    {
        if(CG::IsDelimiter(symbol) || eof())
        {
            if(name.empty())
                Error(ERR_NOT_HEX_CONST, HEX_PREFIX);
            return { NUMBER, val };
        } else if(!CG::IsNumeric(symbol, HEX))
            goto lID;
        name.push_back(Get());
        val *= base;
        val += CG::IsNumeric(symbol) ? symbol - '0' : CG::IsLower(symbol) ? symbol - 'a' + 10 : symbol - 'A' + 10;
        symbol = Peek();
    }

lOct:
    while(true)
    {
        if(CG::IsDelimiter(symbol) || eof())
        {
            if(name.empty())
                Error(ERR_NOT_OCT_CONST, OCT_PREFIX);
            return { NUMBER, val };
        } else if(!CG::IsNumeric(symbol, OCT))
        {
            if(CG::IsNumeric(symbol))
                Error(ERR_NOT_OCT_CONST, name.c_str());
            goto lID;
        }
        name.push_back(Get());
        val *= base;
        val += symbol - '0';
        symbol = Peek();
    }
}

int Lexan::Row() const
{
    return m_LastRow;
}

int Lexan::Col() const
{
    return m_LastCol;
}

void Lexan::SkipWhiteSpace()
{
    while(CharGrouping::IsWhiteSpace(Peek()))
        Get();
}

char Lexan::Get()
{
    char next = m_SourceFile.get();
    m_Col++;
    if(next == '\n')
    {
        m_Col = 1;
        m_Row++;
    }
    return next;
}

char Lexan::Peek()
{
    return m_SourceFile.peek();
}

void Lexan::Error(const std::string& msg, ...)
{
    cerr << "Lexical error:" << m_FileName << ':' << m_Row << ':' << m_Col << ": ";
    va_list args;
    va_start(args, msg);
    vfprintf(stderr, msg.c_str(), args);
    va_end(args);
}

void Lexan::FatalError(const std::string& msg, ...)
{
    va_list args;
    va_start(args, msg);
    Error(msg, args);
    va_end(args);
    exit(1);
}

ostream & operator<<(ostream& stream, const Lexan::LEXEM& e)
{
    stream << '<' << Lexan::s_DebugNames.find(e.Token)->second << '>';
    if(e.Token == Lexan::IDENTIFIER) stream << "(\"" << e.Text << "\")";
    else if(e.Token == Lexan::NUMBER) stream << '(' << e.Val << ')';
    return stream;
}

const map<string, Lexan::TOKEN> Lexan::s_Keywords = {
        { "div",            KW_DIV          },
        { "mod",            KW_MOD          },
        { "and",            KW_AND          },
        { "or",             KW_OR           },
        { "begin",          KW_BEGIN        },
        { "end",            KW_END          },
        { "for",            KW_FOR          },
        { "to",             KW_TO           },
        { "downto",         KW_DOWNTO       },
        { "while",          KW_WHILE        },
        { "do",             KW_DO           },
        { "break",          KW_BREAK        },
        { "exit",           KW_EXIT         },
        { "if",             KW_IF           },
        { "then",           KW_THEN         },
        { "else",           KW_ELSE         },
        { "var",            KW_VAR          },
        { "const",          KW_CONST        },
        { "array",          KW_ARRAY        },
        { "of",             KW_OF           },
        { "program",        KW_PROGRAM      },
        { "function",       KW_FUNCTION     },
        { "procedure",      KW_PROCEDURE    },
        { "forward",        KW_FORWARD      },
        { "integer",        TYPE_INTEGER    }
};

const map<Lexan::TOKEN, string> Lexan::s_DebugNames = {
        { Lexan::IDENTIFIER,       "identifier"    },
        { Lexan::NUMBER,           "number"        },
        { Lexan::SYMB_ADD,         "+"             },
        { Lexan::SYMB_SUB,         "-"             },
        { Lexan::SYMB_MUL,         "*"             },
        { Lexan::SYMB_POW,         "^"             },
        { Lexan::KW_DIV,           "div"           },
        { Lexan::KW_MOD,           "mod"           },
        { Lexan::KW_AND,           "and"           },
        { Lexan::KW_OR,            "or"            },
        { Lexan::SYMB_LT,          "<"             },
        { Lexan::SYMB_LE,          "<="            },
        { Lexan::SYMB_GT,          ">"             },
        { Lexan::SYMB_GE,          ">="            },
        { Lexan::SYMB_EQ,          "="             },
        { Lexan::SYMB_NEQ,         "<>"            },
        { Lexan::SYMB_LPAR,        "("             },
        { Lexan::SYMB_RPAR,        ")"             },
        { Lexan::SYMB_LSB,         "["             },
        { Lexan::SYMB_RSB,         "]"             },
        { Lexan::SYMB_ASS,         ":="            },
        { Lexan::SYMB_COL,         ":"             },
        { Lexan::SYMB_COM,         ","             },
        { Lexan::SYMB_DOT,         "."             },
        { Lexan::SYMB_RNG,         ".."            },
        { Lexan::SYMB_SCOL,        ";"             },
        { Lexan::KW_BEGIN,         "begin"         },
        { Lexan::KW_END,           "end"           },
        { Lexan::KW_FOR,           "for"           },
        { Lexan::KW_TO,            "to"            },
        { Lexan::KW_DOWNTO,        "downto"        },
        { Lexan::KW_WHILE,         "while"         },
        { Lexan::KW_DO,            "do"            },
        { Lexan::KW_BREAK,         "break"         },
        { Lexan::KW_EXIT,          "exit"          },
        { Lexan::KW_IF,            "if"            },
        { Lexan::KW_THEN,          "then"          },
        { Lexan::KW_ELSE,          "else"          },
        { Lexan::KW_VAR,           "var"           },
        { Lexan::KW_CONST,         "const"         },
        { Lexan::KW_ARRAY,         "array"         },
        { Lexan::KW_OF,            "of"            },
        { Lexan::KW_PROGRAM,       "program"       },
        { Lexan::KW_FUNCTION,      "function"      },
        { Lexan::KW_PROCEDURE,     "procedure"     },
        { Lexan::KW_FORWARD,       "forward"       },
        { Lexan::TYPE_INTEGER,     "integer"       },
        { Lexan::UNKNOWN,          "unknown"       },
        { Lexan::END_OF_FILE,      "end of file"   }
};

const char Lexan::OCT_PREFIX = '&';
const char Lexan::HEX_PREFIX = '$';

const char * Lexan::ERR_INVALID_ID      = "%s is NOT valid identifier.";
const char * Lexan::ERR_NOT_OCT_CONST   = "%s  is NOT valid octal constant.";
const char * Lexan::ERR_NOT_HEX_CONST   = "%s is NOT valid hexadecimal constant.";
const char * Lexan::ERR_UNKNOWN_SYMB    = "%c [val: %d] is not known symbol.";

