#ifndef MILA_LEXAN_H
#define MILA_LEXAN_H

#include <fstream>
#include <map>
#include <string>

using BASE = enum Base
{
    OCT = 8,
    DEC = 10,
    HEX = 16
};

class CharGrouping
{
public:
    static bool IsNumeric(char c, BASE b = DEC);
    static bool IsUpper(char c);
    static bool IsLower(char c);
    static bool IsAlpha(char c);
    static bool IsAlphaNumeric(char c);
    static bool IsSpecial(char c);
    static bool IsWhiteSpace(char c);
    static bool IsDelimiter(char c);
    static bool IsUnknown(char c);

private:
    static const char s_Special[];
    static const char s_Whitespace[];
};

class Lexan
{
public:
    using TOKEN = enum LexicalToken
    {
        IDENTIFIER,     // identifier
        NUMBER,         // whole number
        SYMB_ADD,       // +
        SYMB_SUB,       // -
        SYMB_MUL,       // *
        SYMB_POW,       // ^
        KW_DIV,         // div
        KW_MOD,         // mod
        SYMB_LT,        // <
        SYMB_LE,        // <=
        SYMB_GT,        // >
        SYMB_GE,        // >=
        SYMB_EQ,        // =
        SYMB_NEQ,       // <>
        KW_AND,         // and
        KW_OR,          // or
        SYMB_LPAR,      // (
        SYMB_RPAR,      // )
        SYMB_LSB,       // [
        SYMB_RSB,       // ]
        SYMB_ASS,       // :=
        SYMB_COL,       // :
        SYMB_COM,       // ,
        SYMB_DOT,       // .
        SYMB_RNG,       // ..
        SYMB_SCOL,      // ;
        KW_BEGIN,       // begin
        KW_END,         // end
        KW_FOR,         // for
        KW_TO,          // to
        KW_DOWNTO,      // downto
        KW_WHILE,       // while
        KW_DO,          // do
        KW_BREAK,       // break
        KW_EXIT,        // exit
        KW_IF,          // if
        KW_THEN,        // then
        KW_ELSE,        // else
        KW_VAR,         // var
        KW_CONST,       // const
        KW_ARRAY,       // array
        KW_OF,          // of
        KW_PROGRAM,     // program
        KW_FUNCTION,    // function
        KW_PROCEDURE,   // procedure
        KW_FORWARD,     // forward
        TYPE_INTEGER,   // integer
        UNKNOWN,        // unknown symbol
        END_OF_FILE     // end of file
    };
    using LEXEM = struct LexicalElement
    {
        TOKEN Token;
        std::string Text;
        int Val;

        LexicalElement(TOKEN token) : Token(token), Text(), Val() {}
        LexicalElement(TOKEN token, std::string name) : Token(token), Text(std::move(name)), Val() {}
        LexicalElement(TOKEN token, int val) : Token(token), Text(), Val(val) {}
        friend std::ostream& operator<< (std::ostream& stream, const LexicalElement& e);
    };

    explicit Lexan(const std::string& filename);
    ~Lexan();

    bool eof() const;

    LEXEM NextToken();

    int Row() const;
    int Col() const;

private:
    void SkipWhiteSpace();
    char Get();
    char Peek();
    void Error(const std::string& msg, ...);
    void FatalError(const std::string& msg, ...);

    std::ifstream m_SourceFile;
    std::string m_FileName;
    int m_Row, m_Col;
    int m_LastRow, m_LastCol;

    static const std::map<std::string, TOKEN> s_Keywords;

public:
    static const std::map<TOKEN, std::string> s_DebugNames;

private:
    static const char OCT_PREFIX;
    static const char HEX_PREFIX;

    // Error messages
    static const char * ERR_INVALID_ID;
    static const char * ERR_NOT_OCT_CONST;
    static const char * ERR_NOT_HEX_CONST;
    static const char * ERR_UNKNOWN_SYMB;
};

#endif /* MILA_LEXAN_H */