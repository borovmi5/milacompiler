#include "Parser.h"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <memory>

using namespace std;

Parser::Parser(string filename) :
    m_Filename(move(filename)),
    m_Lexan(m_Filename),
    m_Lexem(m_Lexan.NextToken())
{}

Parser::~Parser()
{}

MainUPtr Parser::Parse()
{
    return Program();
}

void Parser::NextToken()
{
    m_Lexem = m_Lexan.NextToken();
}

void Parser::MatchToken(Lexan::TOKEN token)
{
    bool match = m_Lexem.Token == token;
    ostringstream oss;
    oss << "Invalid token at " << m_Lexan.Row() << ':' << m_Lexan.Col() << ". Expected " << Lexan::LEXEM(token) << ", got " << m_Lexem;
    if(!match)
        throw runtime_error(oss.str());
    NextToken();
}

string Parser::MatchIdentifier()
{
    string val = m_Lexem.Text;
    MatchToken(Lexan::IDENTIFIER);
    return move(val);
}

int Parser::MatchNumber()
{
    bool negative = PeekToken() == Lexan::SYMB_SUB;
    if(negative)
        MatchToken(Lexan::SYMB_SUB);
    int val = m_Lexem.Val;
    if(negative)
        val *= -1;
    MatchToken(Lexan::NUMBER);
    return val;
}

Lexan::TOKEN Parser::PeekToken()
{
    return m_Lexem.Token;
}

MainUPtr Parser::Program()
{
    MatchToken(Lexan::KW_PROGRAM);
    auto prg = MatchIdentifier();
    MatchToken(Lexan::SYMB_SCOL);

    vector<DeclarationUPtr> declarations;

    while(true)
    {
        if(PeekToken() == Lexan::KW_CONST)
            for(auto& decl : Constants(true))
                declarations.emplace_back(move(decl));
        else if(PeekToken() == Lexan::KW_VAR)
            for(auto& decl : Variables(true))
                declarations.emplace_back(move(decl));
        else if(PeekToken() == Lexan::KW_PROCEDURE)
            declarations.emplace_back(move(Procedure()));
        else if(PeekToken() == Lexan::KW_FUNCTION)
            declarations.emplace_back(move(Function()));
        else
            break;
    }

    InnerBlockUPtr main = ClosedInnerBlock();
    MatchToken(Lexan::SYMB_DOT);
    return llvm::make_unique<Main>(prg, move(declarations), move(main));
}

vector<ConstantDeclUPtr> Parser::Constants(bool global)
{
    vector<ConstantDeclUPtr> consts;
    MatchToken(Lexan::KW_CONST);
    do
    {
        auto constant = MatchIdentifier();
        MatchToken(Lexan::SYMB_EQ);
        auto value = MatchNumber();
        MatchToken(Lexan::SYMB_SCOL);
        consts.emplace_back(move(llvm::make_unique<ConstantDecl>(constant, value, global)));
    } while(PeekToken() == Lexan::IDENTIFIER);
    return consts;
}

vector<VariableDeclUPtr> Parser::Variables(bool global)
{
    MatchToken(Lexan::KW_VAR);
    vector<VariableDeclUPtr> decl;
    do
    {
        for(auto& var : VariableNames())
            decl.emplace_back(move(var));
        MatchToken(Lexan::SYMB_SCOL);
    } while(PeekToken() == Lexan::IDENTIFIER);
    return decl;
}

vector<VariableDeclUPtr> Parser::Parameters()
{
    vector<VariableDeclUPtr> decl;
    do
    {
        if(decl.size() > 0)
            MatchToken(Lexan::SYMB_SCOL);
        for(auto& var : VariableNames())
            decl.emplace_back(move(var));
    } while(PeekToken() == Lexan::SYMB_SCOL);
    return decl;
}

vector<VariableDeclUPtr> Parser::VariableNames(bool global)
{
    vector<string> names;
    auto var = MatchIdentifier();
    names.emplace_back(move(var));
    while(PeekToken() == Lexan::SYMB_COM)
    {
        MatchToken(Lexan::SYMB_COM);
        var = MatchIdentifier();
        names.emplace_back(move(var));
    }
    MatchToken(Lexan::SYMB_COL);
    auto type = Type();
    vector<VariableDeclUPtr> decl;
    for(auto& name : names)
        decl.emplace_back(llvm::make_unique<VariableDecl>(move(name), type, global));
    return decl;
}

ProcedureUPtr Parser::Procedure()
{
    MatchToken(Lexan::KW_PROCEDURE);
    auto proc = MatchIdentifier();
    MatchToken(Lexan::SYMB_LPAR);
    vector<VariableDeclUPtr> params = Parameters();
    MatchToken(Lexan::SYMB_RPAR);
    MatchToken(Lexan::SYMB_SCOL);
    ProcedureDeclUPtr procedure = llvm::make_unique<ProcedureDecl>(proc, move(params));
    vector<DeclarationUPtr> declarations;
    if(PeekToken() == Lexan::KW_FORWARD)
    {
        MatchToken(Lexan::KW_FORWARD);
        MatchToken(Lexan::SYMB_SCOL);
        return llvm::make_unique<ProcedureImpl>(move(procedure), move(declarations), nullptr);
    }
    while(true)
    {
        if(PeekToken() == Lexan::KW_CONST)
            for(auto& decl : Constants())
                declarations.emplace_back(move(decl));
        else if(PeekToken() == Lexan::KW_VAR)
            for(auto& decl : Variables())
                declarations.emplace_back(move(decl));
        else
            break;
    }
    InnerBlockUPtr inner = move(ClosedInnerBlock());
    MatchToken(Lexan::SYMB_SCOL);
    return llvm::make_unique<ProcedureImpl>(move(procedure), move(declarations), move(inner));
}

FunctionUPtr Parser::Function()
{
    MatchToken(Lexan::KW_FUNCTION);
    auto func = MatchIdentifier();
    MatchToken(Lexan::SYMB_LPAR);
    vector<VariableDeclUPtr> params = Parameters();
    MatchToken(Lexan::SYMB_RPAR);
    MatchToken(Lexan::SYMB_COL);
    TypeSPtr type = Type();
    MatchToken(Lexan::SYMB_SCOL);
    FunctionDeclUPtr function = llvm::make_unique<FunctionDecl>(func, move(params), type);
    vector<DeclarationUPtr> declarations;
    if(PeekToken() == Lexan::KW_FORWARD)
    {
        MatchToken(Lexan::KW_FORWARD);
        MatchToken(Lexan::SYMB_SCOL);
        return llvm::make_unique<FunctionImpl>(move(function), move(declarations), nullptr);
    }
    while(true)
    {
        if(PeekToken() == Lexan::KW_CONST)
            for(auto& decl : Constants())
                declarations.emplace_back(move(decl));
        else if(PeekToken() == Lexan::KW_VAR)
            for(auto& decl : Variables())
                declarations.emplace_back(move(decl));
        else
            break;
    }
    InnerBlockUPtr inner = move(ClosedInnerBlock());
    MatchToken(Lexan::SYMB_SCOL);
    return llvm::make_unique<FunctionImpl>(move(function), move(declarations), move(inner));
}

InnerBlockUPtr Parser::InnerBlock()
{
    if(PeekToken() == Lexan::KW_BEGIN)
        return ClosedInnerBlock();
    else
        return OpenInnerBlock();
}

InnerBlockUPtr Parser::ClosedInnerBlock()
{
    MatchToken(Lexan::KW_BEGIN);
    vector<StatementUPtr> statements = move(Statements());
    MatchToken(Lexan::KW_END);
    return llvm::make_unique<class InnerBlock>(move(statements));
}

InnerBlockUPtr Parser::OpenInnerBlock()
{
    vector<StatementUPtr> statements;
    statements.emplace_back(move(Statement()));
    return llvm::make_unique<class InnerBlock>(move(statements));
}

vector<StatementUPtr> Parser::Statements()
{
    vector<StatementUPtr> statements;
    if(PeekToken() == Lexan::KW_END)
        return statements;
    statements.emplace_back(move(Statement()));
    while(PeekToken() == Lexan::SYMB_SCOL)
    {
        MatchToken(Lexan::SYMB_SCOL);
        if(PeekToken() == Lexan::KW_END)
            break;
        statements.emplace_back(move(Statement()));
    }
    return statements;
}

StatementUPtr Parser::Statement()
{
    StatementUPtr statement;
    switch(PeekToken())
    {
        case Lexan::KW_IF:
            statement = If();
            break;
        case Lexan::KW_WHILE:
            statement = While();
            break;
        case Lexan::KW_FOR:
            statement = For();
            break;
        case Lexan::KW_BREAK:
            statement = Break();
            break;
        case Lexan::KW_EXIT:
            statement = Exit();
            break;
        default:
            statement = NotKWStatement();
            break;
    }
    return statement;
}

StatementUPtr Parser::NotKWStatement()
{
    auto name = MatchIdentifier();
    if(PeekToken() == Lexan::SYMB_LPAR)
        return ProcedureCall(name);
    else
        return Assignment(name);
}

StatementUPtr Parser::ProcedureCall(string name)
{
    MatchToken(Lexan::SYMB_LPAR);
    vector<ExpressionUPtr> params;
    while(PeekToken() != Lexan::SYMB_RPAR)
    {
        if(params.size() > 0)
            MatchToken(Lexan::SYMB_COM);
        params.emplace_back(move(E()));
    }
    MatchToken(Lexan::SYMB_RPAR);
    return llvm::make_unique<class ProcedureCall>(move(name), move(params));
}

StatementUPtr Parser::Assignment(string name)
{
    ReferenceUPtr var = Reference(name);
    MatchToken(Lexan::SYMB_ASS);
    return llvm::make_unique<class Assignment>(move(var), E());
}

StatementUPtr Parser::If()
{
    MatchToken(Lexan::KW_IF);
    ExpressionUPtr condition = E();
    MatchToken(Lexan::KW_THEN);
    InnerBlockUPtr ifBlock = InnerBlock();
    InnerBlockUPtr elseBlock = nullptr;
    if(PeekToken() == Lexan::KW_ELSE)
    {
        MatchToken(Lexan::KW_ELSE);
        elseBlock = move(InnerBlock());
    }
    return llvm::make_unique<class If>(move(condition), move(ifBlock), move(elseBlock));
}

StatementUPtr Parser::While()
{
    MatchToken(Lexan::KW_WHILE);
    ExpressionUPtr condition = E();
    MatchToken(Lexan::KW_DO);
    InnerBlockUPtr whileBlock = InnerBlock();
    return llvm::make_unique<class While>(move(condition), move(whileBlock));
}

StatementUPtr Parser::For()
{
    MatchToken(Lexan::KW_FOR);
    string varName = MatchIdentifier();
    ReferenceUPtr var = Reference(varName);
    MatchToken(Lexan::SYMB_ASS);
    ExpressionUPtr init = E();
    bool to = PeekToken() == Lexan::KW_TO;
    if(to)
        MatchToken(Lexan::KW_TO);
    else
        MatchToken(Lexan::KW_DOWNTO);
    ExpressionUPtr end = E();
    MatchToken(Lexan::KW_DO);
    InnerBlockUPtr forBlock = InnerBlock();
    return llvm::make_unique<class For>(move(var), move(init), move(end), to, move(forBlock));
}

StatementUPtr Parser::Break()
{
    MatchToken(Lexan::KW_BREAK);
    return llvm::make_unique<class Break>();
}

StatementUPtr Parser::Exit()
{
    MatchToken(Lexan::KW_EXIT);
    return llvm::make_unique<class Exit>();
}

ExpressionUPtr Parser::E()
{
    ExpressionUPtr expression = F();
    bool cont = true;
    while(cont)
    {
        switch(PeekToken())
        {
            case Lexan::SYMB_EQ:
                NextToken();
                expression = llvm::make_unique<CmpEq>(move(expression), F());
                break;
            case Lexan::SYMB_NEQ:
                NextToken();
                expression = llvm::make_unique<CmpNeq>(move(expression), F());
                break;
            case Lexan::SYMB_LT:
                NextToken();
                expression = llvm::make_unique<CmpLt>(move(expression), F());
                break;
            case Lexan::SYMB_LE:
                NextToken();
                expression = llvm::make_unique<CmpLeq>(move(expression), F());
                break;
            case Lexan::SYMB_GT:
                NextToken();
                expression = llvm::make_unique<CmpGt>(move(expression), F());
                break;
            case Lexan::SYMB_GE:
                NextToken();
                expression = llvm::make_unique<CmpGeq>(move(expression), F());
                break;
            case Lexan::KW_AND:
                NextToken();
                expression = llvm::make_unique<AndOp>(move(expression), F());
                break;
            case Lexan::KW_OR:
                NextToken();
                expression = llvm::make_unique<OrOp>(move(expression), F());
                break;
            default:
                cont = false;
                break;
        }
    }
    return expression;
}

ExpressionUPtr Parser::F()
{
    ExpressionUPtr expression = G();
    bool cont = true;
    while(cont)
    {
        switch(PeekToken())
        {
            case Lexan::SYMB_ADD:
                NextToken();
                expression = llvm::make_unique<AddOp>(move(expression), G());
                break;
            case Lexan::SYMB_SUB:
                NextToken();
                expression = llvm::make_unique<SubOp>(move(expression), G());
                break;
            default:
                cont = false;
                break;
        }
    }
    return expression;
}

ExpressionUPtr Parser::G()
{
    ExpressionUPtr expression = H();
    bool cont = true;
    while(cont)
    {
        switch(PeekToken())
        {
            case Lexan::SYMB_MUL:
                NextToken();
                expression = llvm::make_unique<MulOp>(move(expression), H());
                break;
            case Lexan::KW_DIV:
                NextToken();
                expression = llvm::make_unique<DivOp>(move(expression), H());
                break;
            case Lexan::KW_MOD:
                NextToken();
                expression = llvm::make_unique<ModOp>(move(expression), H());
                break;
            default:
                cont = false;
                break;
        }
    }
    return expression;
}

ExpressionUPtr Parser::H()
{
    if(PeekToken() == Lexan::NUMBER || PeekToken() == Lexan::SYMB_SUB)
    {
        int val = MatchNumber();
        return llvm::make_unique<Number>(val);
    }
    else if(PeekToken() == Lexan::IDENTIFIER)
    {
        string id = MatchIdentifier();
        if(PeekToken() == Lexan::SYMB_LPAR)
            return FunctionCall(id);
        else
            return Reference(id);
    }
    else
    {
        MatchToken(Lexan::SYMB_LPAR);
        ExpressionUPtr exp = E();
        MatchToken(Lexan::SYMB_RPAR);
        return exp;
    }
}

ExpressionUPtr Parser::FunctionCall(std::string name)
{
    MatchToken(Lexan::SYMB_LPAR);
    vector<ExpressionUPtr> params;
    while(PeekToken() != Lexan::SYMB_RPAR)
    {
        if(params.size() > 0)
            MatchToken(Lexan::SYMB_COM);
        params.emplace_back(move(E()));
    }
    MatchToken(Lexan::SYMB_RPAR);
    return llvm::make_unique<class FunctionCall>(move(name), move(params));
}

ReferenceUPtr Parser::Reference(string name)
{
    ReferenceUPtr ref = llvm::make_unique<PrimitiveRef>(name);
    if(PeekToken() == Lexan::SYMB_LSB)
    {
        MatchToken(Lexan::SYMB_LSB);
        ExpressionUPtr index = E();
        MatchToken(Lexan::SYMB_RSB);
        ref = llvm::make_unique<ArrayRef>(move(ref), move(index));
    }
    return ref;
}

TypeSPtr Parser::Type()
{
    if(PeekToken() == Lexan::KW_ARRAY)
        return Array();
    else
        return PrimitiveType();
}

TypeSPtr Parser::PrimitiveType()
{
    return Integer();
}

TypeSPtr Parser::Integer()
{
    MatchToken(Lexan::TYPE_INTEGER);
    return make_shared<class Integer>();
}

TypeSPtr Parser::Array()
{
    MatchToken(Lexan::KW_ARRAY);
    MatchToken(Lexan::SYMB_LSB);
    auto from = MatchNumber();
    MatchToken(Lexan::SYMB_RNG);
    auto to = MatchNumber();
    MatchToken(Lexan::SYMB_RSB);
    MatchToken(Lexan::KW_OF);
    return make_shared<class Array>(PrimitiveType(), llvm::make_unique<Number>(from), llvm::make_unique<Number>(to));
}