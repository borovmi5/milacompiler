#ifndef MILA_PARSER_H
#define MILA_PARSER_H

#include "Lexan.h"
#include "Tree.h"

#include <string>

class Parser
{
public:
    Parser(std::string filename);
    ~Parser();

    MainUPtr Parse();

private:
    void NextToken();
    void MatchToken(Lexan::TOKEN token);
    std::string MatchIdentifier();
    int MatchNumber();
    Lexan::TOKEN PeekToken();


    MainUPtr Program();

    std::vector<ConstantDeclUPtr> Constants(bool global = false);
    std::vector<VariableDeclUPtr> Variables(bool global = false);
    std::vector<VariableDeclUPtr> Parameters();
    std::vector<VariableDeclUPtr> VariableNames(bool global = false);

    FunctionUPtr Function();
    ProcedureUPtr Procedure();

    InnerBlockUPtr InnerBlock();
    InnerBlockUPtr ClosedInnerBlock();
    InnerBlockUPtr OpenInnerBlock();

    std::vector<StatementUPtr> Statements();
    StatementUPtr Statement();
    StatementUPtr NotKWStatement();
    StatementUPtr ProcedureCall(std::string name);
    StatementUPtr Assignment(std::string name);
    StatementUPtr If();
    StatementUPtr While();
    StatementUPtr For();
    StatementUPtr Break();
    StatementUPtr Exit();

    ExpressionUPtr E();
    ExpressionUPtr F();
    ExpressionUPtr G();
    ExpressionUPtr H();
    ExpressionUPtr FunctionCall(std::string name);

    ReferenceUPtr Reference(std::string name);

    TypeSPtr Type();
    TypeSPtr PrimitiveType();
    TypeSPtr Integer();
    TypeSPtr Array();

    std::string m_Filename;
    Lexan m_Lexan;

    Lexan::LEXEM m_Lexem;
};

#endif /* MILA_PARSER_H */