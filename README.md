# Mila compiler
## Description
Simple Mila compiler front end for LLVM.
## Usage
Mila SRC_FILE \[ -o DEST_FILE \] \[ -c \] \[ -d \] \[ --debug \]

| argument | description |
| --- | --- |
| SRC_FILE | path to source file. Is mandatory. Only one source file allowed. |
| DEST_FILE | path to destination file. Is optional. Only one destination file allowed. |
| -o | specify output file. If not specified "a.out" is used. |
| -c | do not link |
| -d, --debug | debug. Print LLVM module after parse. |

## Limitations
Does not include strings! If you want to compile program, make sure to remove them.

Implements writeln only. write is NOT implemented!

## Samples
Includes 2 custom samples. Samples from [https://gitlab.fit.cvut.cz/pjp/semestralwork/tree/master/samples] could be used
after removing write calls and strings.

## Grammar
Includes file with Mila grammar mila_grammar.grm. Could be
opened in text editor.

## References
\[1\] LLVM documentation: [https://llvm.org/doxygen/]

\[2\] LLVM tutorial: [https://llvm.org/docs/tutorial/]

\[3\] Ivo Strejc, LLVM Sfe compiler: [https://gitlab.fit.cvut.cz/strejivo/BI-PJP/tree/master]

\[4\] cppreference: [https://en.cppreference.com/w/]