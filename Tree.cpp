#include "Tree.h"

#include <iostream>

using namespace std;

static llvm::LLVMContext s_Context;
static llvm::IRBuilder<> s_Builder(s_Context);
static ModuleUPtr s_Module;
static map<string, ValuePtr> s_GlobalConsts;
static map<string, map<string, ValuePtr>> s_LocalConsts;
static map<string, GlobalVarPtr> s_GlobalVars;
static map<string, map<string, AllocaPtr>> s_LocalVars;
static map<string, ValuePtr> s_GlobalMins;
static map<string, map<string, ValuePtr>> s_LocalMins;

static ValuePtr s_NumberFormat;
static ValuePtr s_NewLine;

static BBPtr s_Breakpoint = nullptr;
static BBPtr s_Returnpoint = nullptr;
static bool s_Break = false;

static inline AllocaPtr AllocBlock(FuncPtr function, TypePtr type, const std::string& identifier)
{
    llvm::IRBuilder<> builder(&function->getEntryBlock(), function->getEntryBlock().begin());
    return builder.CreateAlloca(type, llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), type->isArrayTy() ? type->getArrayNumElements() : 0, false), identifier.c_str());
}

InnerBlock::InnerBlock(std::vector<StatementUPtr> statements) :
    m_Statements(move(statements))
{}

void InnerBlock::LLVM() const
{
    for(auto& statement : m_Statements)
    {
        statement->LLVM();
        if(s_Break)
            break;
    }
    s_Break = false;
}

Number::Number(int value) :
    m_Value(value)
{}

ValuePtr Number::Value() const
{
    return llvm::ConstantInt::get(s_Context, llvm::APInt(32, m_Value, true));
}

int Number::GetValue() const
{
    return m_Value;
}

TypePtr Void::GetType() const
{
    return llvm::Type::getVoidTy(s_Context);
}

ConstPtr Void::GetInitValue() const
{
    return llvm::UndefValue::get(llvm::Type::getVoidTy(s_Context));
}

TypePtr Integer::GetType() const
{
    return llvm::Type::getInt32Ty(s_Context);
}

ConstPtr Integer::GetInitValue() const
{
    return llvm::ConstantInt::get(s_Context, llvm::APInt(32, 0, true));
}

Array::Array(TypeSPtr type, NumberUPtr min, NumberUPtr max) :
    m_Type(type),
    m_Min(move(min)),
    m_Max(move(max))
{}

TypePtr Array::GetType() const
{
    return llvm::ArrayType::get(m_Type->GetType(), m_Max->GetValue() - m_Min->GetValue() + 1);
}

ConstPtr Array::GetInitValue() const
{
    return llvm::ConstantArray::get(ArrTypePtr(GetType()), m_Type->GetInitValue());
}

int Array::Min() const
{
    return m_Min->GetValue();
}

int Array::Max() const
{
    return m_Max->GetValue();
}

Declaration::Declaration(bool global) :
    m_Global(global)
{}

ConstantDecl::ConstantDecl(string identifier, int value, bool global) :
    Declaration(global),
    m_Identifier(move(identifier)),
    m_Value(value)
{}

void ConstantDecl::LLVM() const
{
    if(m_Global)
        s_GlobalConsts[m_Identifier] = llvm::ConstantInt::get(s_Context, llvm::APInt(32, m_Value, true));
    else
        s_LocalConsts[s_Builder.GetInsertBlock()->getParent()->getName()][m_Identifier] = llvm::ConstantInt::get(s_Context, llvm::APInt(32, m_Value, true));
}

const string& ConstantDecl::Identifier() const
{
    return m_Identifier;
}

int ConstantDecl::Value() const
{
    return m_Value;
}

VariableDecl::VariableDecl(string identifier, TypeSPtr type, bool global) :
    Declaration(global),
    m_Identifier(move(identifier)),
    m_Type(type)
{}

void VariableDecl::LLVM() const
{
    auto type = m_Type->GetType();
    string pName = s_Builder.GetInsertBlock()->getParent()->getName();
    if(m_Global)
    {
        if(type->isArrayTy())
            s_GlobalMins[m_Identifier] = llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), ArrayPtr(m_Type.get())->Min(), true);
        s_Module->getOrInsertGlobal(m_Identifier, type);
        auto var = s_Module->getNamedGlobal(m_Identifier);
        var->setLinkage(llvm::GlobalValue::CommonLinkage);
        var->setAlignment(type->isArrayTy() ? 16 : 4);
        var->setInitializer(m_Type->GetInitValue());
        s_GlobalVars[m_Identifier] = var;
    }
    else
    {
        if(type->isArrayTy())
            s_LocalMins[pName][m_Identifier] = llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), ArrayPtr(m_Type.get())->Min(), true);
        s_LocalVars[pName][m_Identifier] = s_Builder.CreateAlloca(type, nullptr, m_Identifier);
    }
}

const string& VariableDecl::Identifier() const
{
    return m_Identifier;
}

const TypeSPtr& VariableDecl::Type() const
{
    return m_Type;
}

FunctionDecl::FunctionDecl(string identifier, vector<VariableDeclUPtr> params, TypeSPtr type) :
        m_Identifier(move(identifier)),
        m_Params(move(params)),
        m_Type(type)
{}

FuncPtr FunctionDecl::Generate()
{
    vector<TypePtr> params;
    for(auto& param : m_Params)
        params.push_back(param->Type()->GetType());
    FuncTypePtr type = llvm::FunctionType::get(m_Type->GetType(), params, false);
    FuncPtr func = llvm::Function::Create(type, llvm::Function::ExternalLinkage, m_Identifier, s_Module.get());
    int idx = 0;
    for(auto& arg : func->args())
        arg.setName(m_Params[idx++]->Identifier());
    return func;
}

const string& FunctionDecl::Identifier() const
{
    return m_Identifier;
}

TypeSPtr FunctionDecl::Type() const
{
    return m_Type;
}

FunctionImpl::FunctionImpl(FunctionDeclUPtr declaration, vector<DeclarationUPtr> declarations, InnerBlockUPtr inner) :
    Declaration(),
    m_Declaration(move(declaration)),
    m_Declarations(move(declarations)),
    m_Inner(move(inner))
{}

void FunctionImpl::LLVM() const
{
    FuncPtr func = s_Module->getFunction(m_Declaration->Identifier());
    if(!func) func = m_Declaration->Generate();
    if(!func || !m_Inner) return;
    BBPtr block = llvm::BasicBlock::Create(s_Context, "func" + m_Declaration->Identifier() + "b", func);
    BBPtr endBlock = llvm::BasicBlock::Create(s_Context, "func" + m_Declaration->Identifier() + "end");
    func->getBasicBlockList().push_back(endBlock);
    auto pblock = s_Builder.GetInsertBlock();
    s_Builder.SetInsertPoint(block);
    s_LocalVars[m_Declaration->Identifier()][m_Declaration->Identifier()] = AllocBlock(func, m_Declaration->Type()->GetType(), m_Declaration->Identifier());
    for(auto& arg : func->args())
    {
        AllocaPtr alloc = AllocBlock(func, arg.getType(), arg.getName());
        s_Builder.CreateStore(&arg, alloc);
        s_LocalVars[m_Declaration->Identifier()][arg.getName()] = alloc;
    }
    for(auto& decl : m_Declarations)
        decl->LLVM();
    auto prevReturnpoint = s_Returnpoint;
    s_Returnpoint = endBlock;
    m_Inner->LLVM();
    s_Builder.CreateBr(endBlock);
    s_Builder.SetInsertPoint(endBlock);
    s_Returnpoint = prevReturnpoint;
    s_Builder.CreateRet(s_Builder.CreateLoad(s_LocalVars[m_Declaration->Identifier()][m_Declaration->Identifier()]));
    s_Builder.SetInsertPoint(pblock);
}

ProcedureDecl::ProcedureDecl(string identifier, vector<VariableDeclUPtr> params) :
    FunctionDecl(move(identifier), move(params), TypeSPtr(new Void()))
{}

ProcedureImpl::ProcedureImpl(ProcedureDeclUPtr declaration, vector<DeclarationUPtr> declarations, InnerBlockUPtr inner) :
    Declaration(),
    m_Declaration(move(declaration)),
    m_Declarations(move(declarations)),
    m_Inner(move(inner))
{}

void ProcedureImpl::LLVM() const
{
    FuncPtr func = s_Module->getFunction(m_Declaration->Identifier());
    if(!func) func = m_Declaration->Generate();
    if(!func || !m_Inner) return;
    BBPtr block = llvm::BasicBlock::Create(s_Context, "proc" + m_Declaration->Identifier() + "b", func);
    BBPtr endBlock = llvm::BasicBlock::Create(s_Context, "proc" + m_Declaration->Identifier() + "end");
    func->getBasicBlockList().push_back(endBlock);
    auto pblock = s_Builder.GetInsertBlock();
    s_Builder.SetInsertPoint(block);
    for(auto& arg : func->args())
    {
        AllocaPtr alloc = AllocBlock(func, arg.getType(), arg.getName());
        s_Builder.CreateStore(&arg, alloc);
        s_LocalVars[m_Declaration->Identifier()][arg.getName()] = alloc;
    }
    for(auto& decl : m_Declarations)
        decl->LLVM();
    auto prevReturnpoint = s_Returnpoint;
    s_Returnpoint = endBlock;
    m_Inner->LLVM();
    s_Builder.CreateBr(endBlock);
    s_Builder.SetInsertPoint(endBlock);
    s_Returnpoint = prevReturnpoint;
    s_Builder.CreateRetVoid();
    s_Builder.SetInsertPoint(pblock);
}

PrimitiveRef::PrimitiveRef(string identifier) :
    m_Identifier(move(identifier))
{}

ValuePtr PrimitiveRef::Value() const
{
    string pblock = s_Builder.GetInsertBlock()->getParent()->getName();
    if(s_LocalConsts[pblock].find(m_Identifier) != s_LocalConsts[pblock].end())
        return s_LocalConsts[pblock].at(m_Identifier);
    if(s_LocalVars[pblock].find(m_Identifier) != s_LocalVars[pblock].end())
        return s_Builder.CreateLoad(s_LocalVars[pblock].at(m_Identifier), m_Identifier);
    if(s_GlobalConsts.find(m_Identifier) != s_GlobalConsts.end())
        return s_GlobalConsts.at(m_Identifier);
    if(s_GlobalVars.find(m_Identifier) != s_GlobalVars.end())
        return s_Builder.CreateLoad(s_GlobalVars.at(m_Identifier), m_Identifier);
    throw runtime_error("Variable or const " + m_Identifier + " used but not defined.");
}

ValuePtr PrimitiveRef::Address() const
{
    string pblock = s_Builder.GetInsertBlock()->getParent()->getName();
    if(s_LocalVars[pblock].find(m_Identifier) != s_LocalVars[pblock].end())
        return s_LocalVars[pblock].at(m_Identifier);
    if(s_GlobalVars.find(m_Identifier) != s_GlobalVars.end())
        return s_GlobalVars.at(m_Identifier);
    throw runtime_error("Variable " + m_Identifier + " used but not defined.");
}

const string& PrimitiveRef::Identifier() const
{
    return m_Identifier;
}

ArrayRef::ArrayRef(ReferenceUPtr array, ExpressionUPtr idx) :
    m_Array(move(array)),
    m_Index(move(idx))
{}

ValuePtr ArrayRef::Value() const
{
    return s_Builder.CreateLoad(Address());
}

ValuePtr ArrayRef::Address() const
{
    string arr = PrimitiveRefPtr(m_Array.get())->Identifier();
    string pblock = s_Builder.GetInsertBlock()->getParent()->getName();
    if(s_LocalVars[pblock].find(arr) != s_LocalVars[pblock].end()){
        vector<ValuePtr> idxList;
        idxList.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), 0));
        idxList.push_back(s_Builder.CreateSub(m_Index->Value(), s_LocalMins[pblock][arr]));
        return s_Builder.CreateGEP(s_LocalVars[pblock].at(arr), idxList);
    }
    if(s_GlobalVars.find(arr) != s_GlobalVars.end()){
        vector<ValuePtr> idxList;
        idxList.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), 0));
        idxList.push_back(s_Builder.CreateSub(m_Index->Value(), s_GlobalMins[arr]));
        return s_Builder.CreateGEP(s_GlobalVars.at(arr), idxList);
    }
    return nullptr;
}

FunctionCall::FunctionCall(string identifier, vector<ExpressionUPtr> params) :
    m_Identifier(move(identifier)),
    m_Params(move(params))
{}

ValuePtr FunctionCall::Value() const
{
    FuncPtr func = s_Module->getFunction(m_Identifier);
    if(!func)
        throw runtime_error("Function " + m_Identifier + " not defined.");
    if(func->arg_size() != m_Params.size())
        throw runtime_error("Function " + m_Identifier + " expects " + to_string(m_Params.size()) + " but got " + to_string(func->arg_size()) + "." );
    vector<ValuePtr> args;
    for(auto& param : m_Params)
        args.push_back(param->Value());
    return s_Builder.CreateCall(func, args);
}

ProcedureCall::ProcedureCall(string identifier, vector<ExpressionUPtr> params) :
    m_Identifier(move(identifier)),
    m_Params(move(params))
{}

void ProcedureCall::LLVM() const
{
    if(m_Identifier == "writeln")
    {
        vector<ValuePtr> params = { s_NumberFormat, m_Params[0]->Value() };
        s_Builder.CreateCall(s_Module->getFunction("printf"), params);
        s_Builder.CreateCall(s_Module->getFunction("printf"), { s_NewLine });
    }
    else if(m_Identifier == "readln")
    {
        ValuePtr address = ReferencePtr(m_Params[0].get())->Address();
        vector<ValuePtr> params = { s_NumberFormat, address };
        s_Builder.CreateCall(s_Module->getFunction("scanf"), params);
    }
    else if(m_Identifier == "inc")
    {
        ValuePtr val = m_Params[0]->Value();
        ValuePtr address = ReferencePtr(m_Params[0].get())->Address();
        s_Builder.CreateStore(s_Builder.CreateAdd(val, llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), 1, true)), address);
    }
    else if(m_Identifier == "dec")
    {
        ValuePtr val = m_Params[0]->Value();
        ValuePtr address = ReferencePtr(m_Params[0].get())->Address();
        s_Builder.CreateStore(s_Builder.CreateSub(val, llvm::ConstantInt::get(llvm::Type::getInt32Ty(s_Context), 1, true)), address);
    }
    else
    {
        FuncPtr func = s_Module->getFunction(m_Identifier);
        if(!func)
            throw runtime_error("Procedure " + m_Identifier + " not defined.");
        if(func->arg_size() != m_Params.size())
            throw runtime_error("Procedure " + m_Identifier + " expects " + to_string(m_Params.size()) + " but got " + to_string(func->arg_size()) + "." );
        vector<ValuePtr> args;
        for(auto& param : m_Params)
            args.push_back(param->Value());
        s_Builder.CreateCall(func, args);
    }
}

Assignment::Assignment(ReferenceUPtr variable, ExpressionUPtr value) :
    m_Variable(move(variable)),
    m_Value(move(value))
{}

void Assignment::LLVM() const
{
    ValuePtr address = m_Variable->Address();
    s_Builder.CreateStore(m_Value->Value(), address);
}

If::If(ExpressionUPtr condition, InnerBlockUPtr ifBlock, InnerBlockUPtr elseBlock) :
    m_Condition(move(condition)),
    m_If(move(ifBlock)),
    m_Else(move(elseBlock))
{}

void If::LLVM() const
{
    FuncPtr func = s_Builder.GetInsertBlock()->getParent();
    BBPtr ifBlock = llvm::BasicBlock::Create(s_Context, "ifThen", func);
    BBPtr elseBlock = llvm::BasicBlock::Create(s_Context, "ifElse");
    func->getBasicBlockList().push_back(elseBlock);
    BBPtr afterBlock = llvm::BasicBlock::Create(s_Context, "ifAfter");
    func->getBasicBlockList().push_back(afterBlock);
    ValuePtr condition = s_Builder.CreateICmpNE(m_Condition->Value(), llvm::ConstantInt::get(s_Context, llvm::APInt(32, 0, true)));
    s_Builder.CreateCondBr(condition, ifBlock, elseBlock);
    s_Builder.SetInsertPoint(ifBlock);
    m_If->LLVM();
    s_Builder.CreateBr(afterBlock);
    s_Builder.SetInsertPoint(elseBlock);
    if(m_Else)
        m_Else->LLVM();
    s_Builder.CreateBr(afterBlock);
    s_Builder.SetInsertPoint(afterBlock);
}

While::While(ExpressionUPtr condition, InnerBlockUPtr whileBlock) :
    m_Condition(move(condition)),
    m_While(move(whileBlock))
{}

void While::LLVM() const
{
    FuncPtr func = s_Builder.GetInsertBlock()->getParent();
    BBPtr conditionBlock = llvm::BasicBlock::Create(s_Context, "whileCond", func);
    BBPtr bodyBlock = llvm::BasicBlock::Create(s_Context, "whileBody");
    func->getBasicBlockList().push_back(bodyBlock);
    BBPtr afterBlock = llvm::BasicBlock::Create(s_Context, "whileAfter");
    func->getBasicBlockList().push_back(afterBlock);
    s_Builder.CreateBr(conditionBlock);
    s_Builder.SetInsertPoint(conditionBlock);
    ValuePtr condition = s_Builder.CreateICmpNE(m_Condition->Value(), llvm::ConstantInt::get(s_Context, llvm::APInt(32, 0, true)));
    s_Builder.CreateCondBr(condition, bodyBlock, afterBlock);
    s_Builder.SetInsertPoint(bodyBlock);
    BBPtr prevBreakpoint = s_Breakpoint;
    s_Breakpoint = afterBlock;
    m_While->LLVM();
    s_Breakpoint = prevBreakpoint;
    s_Builder.CreateBr(conditionBlock);
    s_Builder.SetInsertPoint(afterBlock);
}

For::For(ReferenceUPtr variable, ExpressionUPtr init, ExpressionUPtr end, bool to, InnerBlockUPtr forBlock) :
    m_Variable(move(variable)),
    m_Init(move(init)),
    m_End(move(end)),
    m_To(to),
    m_For(move(forBlock))
{}

void For::LLVM() const
{
    FuncPtr func = s_Builder.GetInsertBlock()->getParent();
    BBPtr forBlock = llvm::BasicBlock::Create(s_Context, "forBody", func);
    BBPtr afterBlock = llvm::BasicBlock::Create(s_Context, "forAfter");
    func->getBasicBlockList().push_back(afterBlock);
    string pblock = func->getName();
    ValuePtr init = m_Init->Value();
    if(!init)
        throw runtime_error("For loop invalid init value.");
    s_Builder.CreateStore(init, m_Variable->Address());
    s_Builder.CreateBr(forBlock);
    s_Builder.SetInsertPoint(forBlock);
    BBPtr prevBreakpoint = s_Breakpoint;
    s_Breakpoint = afterBlock;
    m_For->LLVM();
    s_Breakpoint = prevBreakpoint;
    ValuePtr step = llvm::ConstantInt::get(s_Context, llvm::APInt(32, 1, true));
    ValuePtr end = m_End->Value();
    if(!end)
        throw runtime_error("For loop invalid end value.");
    ValuePtr cur = s_Builder.CreateLoad(m_Variable->Address(), "curValue");
    ValuePtr next = m_To ? s_Builder.CreateAdd(cur, step, "nextValue") : s_Builder.CreateSub(cur, step, "nextValue");
    s_Builder.CreateStore(next, m_Variable->Address());
    ValuePtr endCond = m_To ? s_Builder.CreateICmpSLT(cur, m_End->Value()) : s_Builder.CreateICmpSGT(cur, m_End->Value());
    s_Builder.CreateCondBr(endCond, forBlock, afterBlock);
    s_Builder.SetInsertPoint(afterBlock);
}

void Break::LLVM() const
{
    if(!s_Breakpoint)
        throw runtime_error("Break not allowed here.");
    s_Builder.CreateBr(s_Breakpoint);
    s_Break = true;
}

void Exit::LLVM() const
{
    if(!s_Returnpoint)
        throw runtime_error("Exit not allowed here.");
    s_Builder.CreateBr(s_Returnpoint);
    s_Break = true;
}

BinaryOperation::BinaryOperation(ExpressionUPtr a, ExpressionUPtr b) :
    m_A(move(a)),
    m_B(move(b))
{}

ValuePtr AddOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateAdd(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr SubOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateSub(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr MulOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateMul(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr DivOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateSDiv(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr ModOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateSRem(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr CmpEq::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateICmpEQ(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr CmpNeq::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateICmpNE(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr CmpLt::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateICmpSLT(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr CmpLeq::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateICmpSLE(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr CmpGt::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateICmpSGT(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr CmpGeq::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateICmpSGE(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr AndOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateAnd(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

ValuePtr OrOp::Value() const
{
    ValuePtr a = m_A->Value();
    ValuePtr b = m_B->Value();
    if(!a || !b) return nullptr;
    return s_Builder.CreateIntCast(s_Builder.CreateOr(a, b), llvm::Type::getInt32Ty(s_Context), true);
}

Main::Main(string identifier, vector<DeclarationUPtr> declaration, InnerBlockUPtr inner) :
    m_Identifier(move(identifier)),
    m_Declarations(move(declaration)),
    m_Inner(move(inner))
{}

void Main::LLVM() const
{
    FuncPtr write = llvm::Function::Create(llvm::FunctionType::get(llvm::Type::getInt32Ty(s_Context), {llvm::Type::getInt8PtrTy(s_Context)}, true), llvm::Function::ExternalLinkage, llvm::Twine("printf"), s_Module.get());
    FuncPtr read  = llvm::Function::Create(llvm::FunctionType::get(llvm::Type::getInt32Ty(s_Context), {llvm::Type::getInt8PtrTy(s_Context)}, true), llvm::Function::ExternalLinkage, llvm::Twine("scanf"), s_Module.get());
    write->setCallingConv(llvm::CallingConv::C);
    read->setCallingConv(llvm::CallingConv::C);

    FuncPtr main = llvm::Function::Create(llvm::FunctionType::get(llvm::Type::getInt32Ty(s_Context), {}, false), llvm::GlobalValue::ExternalLinkage, "main", s_Module.get());
    main->setCallingConv(llvm::CallingConv::C);
    BBPtr block = llvm::BasicBlock::Create(s_Context, "mainb", main);
    BBPtr mainEnd = llvm::BasicBlock::Create(s_Context, "mainEnd");
    main->getBasicBlockList().push_back(mainEnd);
    s_Builder.SetInsertPoint(block);
    s_NumberFormat = s_Builder.CreateGlobalStringPtr("%d");
    s_NewLine = s_Builder.CreateGlobalStringPtr("\n");
    for(auto& decl : m_Declarations)
        decl->LLVM();
    s_Returnpoint = mainEnd;
    m_Inner->LLVM();
    s_Builder.CreateBr(mainEnd);
    s_Builder.SetInsertPoint(mainEnd);
    s_Returnpoint = nullptr;
    s_Builder.CreateRet(llvm::ConstantInt::get(s_Context, llvm::APInt(32, 0)));
}

void Main::GetLLVM() const
{
    s_Module = llvm::make_unique<llvm::Module>("", s_Context);
    LLVM();
}

int Main::Save(const string& filename)
{
    llvm::InitializeAllTargetInfos();
    llvm::InitializeAllTargets();
    llvm::InitializeAllTargetMCs();
    llvm::InitializeAllAsmParsers();
    llvm::InitializeAllAsmPrinters();

    auto targetTriple = llvm::sys::getDefaultTargetTriple();
    string err;
    auto target = llvm::TargetRegistry::lookupTarget(targetTriple, err);
    if(!target)
    {
        llvm::errs() << err;
        return 1;
    }
    auto cpu = "generic";
    auto features = "";
    llvm::TargetOptions options;
    auto machine = target->createTargetMachine(targetTriple, cpu, features, options, llvm::Reloc::Model::PIC_);
    s_Module->setDataLayout(machine->getDataLayout());
    s_Module->setTargetTriple(targetTriple);
    error_code ec;
    llvm::raw_fd_ostream dest(filename, ec, llvm::sys::fs::F_None);
    if(ec)
    {
        llvm::errs() << "Could not open file: " << ec.message();
        return 1;
    }
    llvm::legacy::PassManager manager;
    auto ft = llvm::TargetMachine::CGFT_ObjectFile;
    llvm::formatted_raw_ostream fros(dest);
    if(machine->addPassesToEmitFile(manager, fros, ft))
    {
        llvm::errs() << "Machine can't emit object file.";
        return 1;
    }
    manager.run(*s_Module);
    dest.flush();
    return 0;
}

void Main::Print() const
{
    s_Module->print(llvm::outs(), nullptr);
}
