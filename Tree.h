#ifndef MILA_TREE_H
#define MILA_TREE_H

#include <memory>
#include <vector>

#include <llvm/ADT/APFloat.h>
#include <llvm/ADT/IndexedMap.h>
#include <llvm/ADT/Optional.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/GenericValue.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Value.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Target/TargetOptions.h>

using AllocaPtr = llvm::AllocaInst *;
using BBPtr = llvm::BasicBlock *;
using ConstPtr = llvm::Constant *;
using FuncPtr = llvm::Function *;
using GlobalVarPtr = llvm::GlobalVariable *;
using ModuleUPtr = std::unique_ptr<llvm::Module>;
using ValuePtr = llvm::Value *;
using ValueUPtr = std::unique_ptr<llvm::Value>;
using TypePtr = llvm::Type *;
using ArrTypePtr = llvm::ArrayType *;
using FuncTypePtr = llvm::FunctionType *;

class Node
{
public:
    Node() = default;
    virtual ~Node() = default;
};

class Expression : public Node
{
public:
    Expression() = default;
    virtual ValuePtr Value() const = 0;
};

using ExpressionUPtr = std::unique_ptr<Expression>;

class Statement : public Node
{
public:
    Statement() = default;
    virtual void LLVM() const = 0;
};

using StatementUPtr = std::unique_ptr<Statement>;

class InnerBlock : public Node
{
public:
    InnerBlock(std::vector<StatementUPtr> statements);
    void LLVM() const;

private:
    const std::vector<StatementUPtr> m_Statements;
};

using InnerBlockUPtr = std::unique_ptr<InnerBlock>;

class Number : public Expression
{
public:
    Number(int value);
    ValuePtr Value() const override;
    int GetValue() const;

private:
    int m_Value;
};

using NumberUPtr = std::unique_ptr<Number>;

class Type : public Node
{
public:
    Type() = default;
    virtual TypePtr GetType() const = 0;
    virtual ConstPtr GetInitValue() const = 0;
};

using TypeSPtr = std::shared_ptr<Type>;

class Void : public Type
{
public:
    Void() = default;
    TypePtr GetType() const override;
    ConstPtr GetInitValue() const override;
};

class Integer : public Type
{
public:
    Integer() = default;
    TypePtr GetType() const override;
    ConstPtr GetInitValue() const override;
};

class Array : public Type
{
public:
    Array(TypeSPtr type, NumberUPtr min, NumberUPtr max);
    TypePtr GetType() const override;
    ConstPtr GetInitValue() const override;
    int Min() const;
    int Max() const;

private:
    const TypeSPtr m_Type;
    const NumberUPtr m_Min, m_Max;
};

using ArrayPtr = Array *;

class Declaration : public Node
{
public:
    Declaration(bool global = true);
    virtual void LLVM() const = 0;

protected:
    bool m_Global;
};

using DeclarationUPtr = std::unique_ptr<Declaration>;

class ConstantDecl : public Declaration
{
public:
    ConstantDecl(std::string identifier, int value, bool global = true);
    void LLVM() const override;
    const std::string& Identifier() const;
    int Value() const;

private:
    const std::string m_Identifier;
    const int m_Value;
};

using ConstantDeclUPtr = std::unique_ptr<ConstantDecl>;

class VariableDecl : public Declaration
{
public:
    VariableDecl(std::string identifier, TypeSPtr type, bool global = true);
    void LLVM() const override;
    const std::string& Identifier() const;
    const TypeSPtr& Type() const;

private:
    const std::string m_Identifier;
    const TypeSPtr m_Type;
};

using VariableDeclUPtr = std::unique_ptr<VariableDecl>;

class FunctionDecl
{
public:
    FunctionDecl(std::string identifier, std::vector<VariableDeclUPtr> params, TypeSPtr type);
    FuncPtr Generate();
    const std::string& Identifier() const;
    TypeSPtr Type() const;

protected:
    const std::string m_Identifier;
    const std::vector<VariableDeclUPtr> m_Params;
    const TypeSPtr m_Type;
};

using FunctionDeclUPtr = std::unique_ptr<FunctionDecl>;

class FunctionImpl : public Declaration
{
public:
    FunctionImpl(FunctionDeclUPtr declaration, std::vector<DeclarationUPtr> declarations, InnerBlockUPtr inner);
    void LLVM() const override;

private:
    const FunctionDeclUPtr m_Declaration;
    const std::vector<DeclarationUPtr> m_Declarations;
    const InnerBlockUPtr m_Inner;
};

using FunctionUPtr = std::unique_ptr<FunctionImpl>;

class ProcedureDecl : public FunctionDecl
{
public:
    ProcedureDecl(std::string identifier, std::vector<VariableDeclUPtr> params);
};

using ProcedureDeclUPtr = std::unique_ptr<ProcedureDecl>;

class ProcedureImpl : public Declaration
{
public:
    ProcedureImpl(ProcedureDeclUPtr declaration, std::vector<DeclarationUPtr> declarations, InnerBlockUPtr inner);
    void LLVM() const override;

private:
    const ProcedureDeclUPtr m_Declaration;
    const std::vector<DeclarationUPtr> m_Declarations;
    const InnerBlockUPtr m_Inner;
};

using ProcedureUPtr = std::unique_ptr<ProcedureImpl>;

class Reference : public Expression
{
public:
    Reference() = default;
    virtual ValuePtr Value() const = 0;
    virtual ValuePtr Address() const = 0;
};

using ReferencePtr = Reference *;
using ReferenceUPtr = std::unique_ptr<Reference>;

class PrimitiveRef : public Reference
{
public:
    PrimitiveRef(std::string identifier);
    ValuePtr Value() const override;
    ValuePtr Address() const override;
    const std::string& Identifier() const;

private:
    const std::string m_Identifier;
};

using PrimitiveRefPtr = PrimitiveRef *;

class ArrayRef : public Reference
{
public:
    ArrayRef(ReferenceUPtr array, ExpressionUPtr idx);
    ValuePtr Value() const override;
    ValuePtr Address() const override;
    int Index() const;

private:
    const ReferenceUPtr m_Array;
    const ExpressionUPtr m_Index;
};

using ArrayRefPtr = ArrayRef *;

class FunctionCall : public Expression
{
public:
    FunctionCall(std::string identifier, std::vector<ExpressionUPtr> params);
    ValuePtr Value() const override;

private:
    const std::string m_Identifier;
    const std::vector<ExpressionUPtr> m_Params;
};

class ProcedureCall : public Statement
{
public:
    ProcedureCall(std::string identifier, std::vector<ExpressionUPtr> params);
    void LLVM() const override;

private:
    const std::string m_Identifier;
    const std::vector<ExpressionUPtr> m_Params;
};

class Assignment : public Statement
{
public:
    Assignment(ReferenceUPtr variable, ExpressionUPtr value);
    void LLVM() const override;

private:
    const ReferenceUPtr m_Variable;
    const ExpressionUPtr m_Value;
};

class If : public Statement
{
public:
    If(ExpressionUPtr condition, InnerBlockUPtr ifBlock, InnerBlockUPtr elseBlock);
    void LLVM() const override;

private:
    const ExpressionUPtr m_Condition;
    const InnerBlockUPtr m_If;
    const InnerBlockUPtr m_Else;
};

class While : public Statement
{
public:
    While(ExpressionUPtr condition, InnerBlockUPtr whileBlock);
    void LLVM() const override;

private:
    const ExpressionUPtr m_Condition;
    const InnerBlockUPtr m_While;
};

class For : public Statement
{
public:
    For(ReferenceUPtr variable, ExpressionUPtr init, ExpressionUPtr end, bool to, InnerBlockUPtr forBlock);
    void LLVM() const override;

private:
    const ReferenceUPtr m_Variable;
    const ExpressionUPtr m_Init;
    const ExpressionUPtr m_End;
    const bool m_To;
    const InnerBlockUPtr m_For;
};

class Break : public Statement
{
public:
    Break() = default;
    void LLVM() const override;
};

class Exit : public Statement
{
public:
    Exit() = default;
    void LLVM() const override;
};

class BinaryOperation : public Expression
{
public:
    BinaryOperation(ExpressionUPtr a, ExpressionUPtr b);

protected:
    const ExpressionUPtr m_A, m_B;
};

class AddOp : public BinaryOperation
{
public:
    AddOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class SubOp : public BinaryOperation
{
public:
    SubOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class MulOp : public BinaryOperation
{
public:
    MulOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class DivOp : public BinaryOperation
{
public:
    DivOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class ModOp : public BinaryOperation
{
public:
    ModOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class CmpEq : public BinaryOperation
{
public:
    CmpEq(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class CmpNeq : public BinaryOperation
{
public:
    CmpNeq(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class CmpLt : public BinaryOperation
{
public:
    CmpLt(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class CmpLeq : public BinaryOperation
{
public:
    CmpLeq(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class CmpGt : public BinaryOperation
{
public:
    CmpGt(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class CmpGeq : public BinaryOperation
{
public:
    CmpGeq(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class AndOp : public BinaryOperation
{
public:
    AndOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class OrOp : public BinaryOperation
{
public:
    OrOp(ExpressionUPtr a, ExpressionUPtr b) : BinaryOperation(move(a), move(b)) {}
    ValuePtr Value() const override;
};

class Main : public Node
{
public:
    Main(std::string identifier, std::vector<DeclarationUPtr> declaration, InnerBlockUPtr inner);

    void LLVM() const;
    void GetLLVM() const;
    void Print() const;
    int Save(const std::string& filename);

private:
    const std::string m_Identifier;
    const std::vector<DeclarationUPtr> m_Declarations;
    const InnerBlockUPtr m_Inner;
};

using MainUPtr = std::unique_ptr<Main>;

#endif /* MILA_TREE_H */