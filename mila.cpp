#include <iostream>
#include <memory>

#include "Parser.h"

using namespace std;

int main(int argc, char* argv[])
{
    const char * inputFile = nullptr;
    const char * outputFile = nullptr;
    bool debug = false;
    bool link = true;
    for(int i = 1; i < argc; ++i)
    {
        if(!strcmp(argv[i], "-o"))
        {
            if(i + 1 >= argc)
            {
                cerr << "Error: Unspecified output file." << endl;
                return 1;
            }
            if(outputFile)
            {
                cerr << "Error: Output file specified already." << endl;
                return 1;
            }
            outputFile = argv[++i];
        }
        else if(!strcmp(argv[i], "--debug"))
            debug = true;
        else if(argv[i][0] == '-')
        {
            for(unsigned int j = 1; j < strlen(argv[i]); ++j)
            {
                switch(argv[i][j])
                {
                    case 'd':
                        debug = true;
                        break;
                    case 'c':
                        link = false;
                        break;
                    default:
                        cerr << "Error: unknown option -" << argv[i][j] << '.' << endl;
                        return 1;
                }
            }
        }
        else if(inputFile)
        {
            cerr << "Error: Input file specified already." << endl;
            return 1;
        }
        else
            inputFile = argv[i];
    }

    if(!outputFile)
        outputFile = "a.out";
    string outLLVM(outputFile);
    if(link)
        outLLVM += ".tmp";

    if(!inputFile)
    {
        cerr << "Error: Input file not specified." << endl;
        return 1;
    }

    try
    {
        Parser parser(inputFile);
        MainUPtr main = parser.Parse();
        main->GetLLVM();
        if(debug) main->Print();
        main->Save(outLLVM);
        if(link)
        {
            system(("clang " + outLLVM + " -o " + outputFile).c_str());
            system(("rm " + outLLVM).c_str());
        }
    }
    catch(exception& e)
    {
        cerr << e.what() << endl;
        return 1;
    }

    return 0;
}

